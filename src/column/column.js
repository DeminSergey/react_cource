import React from 'react'
import Cell from '.././cell/cell.js'

function Column(props) {
    return (
        <div onClick={()=> props.onColumnPress(props.columnId)}>
            {
                props.data.map(function (item,index) {
                    return(
                        <Cell
                            data={item}
                            key={index}
                        />
                    )
                })
            }
        </div>
    )
}

export default Column;