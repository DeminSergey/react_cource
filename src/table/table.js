import React from 'react'
import Column from '.././column/column.js'
import './table.css'

function Table(props) {
    return (
        <div>
            Ход игрока : {props.currentPlayer}
            <div className={'column-wrapper'}>
                {
                    props.field.map(function (item,index) {
                        return (
                            <Column
                                columnId={index}
                                key={index}
                                onColumnPress={props.onColumnPress}
                                data={props.field[index]}
                            />
                        )
                    })
                }

            </div>
        </div>
    )
}

export default  Table;