import React from 'react'
import './cell.css'

function Cell(props) {
    return (
        <div className={'cell-wrapper'}>
            {props.data}
        </div>
    )
}

export default Cell
