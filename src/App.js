import React , { useState } from 'react';
import './App.css';
import Table from './table/table.js'


function App() {

  const [currentPlayer, setCurrentPlayer] = useState(2);
  const [field, setField] = useState([
      [1,0,0,0,0,0],
      [2,1,0,0,0,0],
      [2,0,0,0,0,0],
      [1,0,0,0,0,0],
      [0,1,0,0,0,0],
      [2,0,0,0,0,0],
      [1,1,0,0,0,0]
  ]);




  const winner = 0;

  function move(columnId) {
      setCurrentPlayer(1);
      setField([
          [1,1,1,1,1,1],
          [2,1,0,0,0,0],
          [2,0,0,0,0,0],
          [1,0,0,0,0,0],
          [0,1,0,0,0,0],
          [2,0,0,0,0,0],
          [1,1,0,0,0,0]
      ])
  }
  return (
    <div className="App">
      <Table
          onColumnPress={move}
          currentPlayer={currentPlayer}
          field={field}
      />
    </div>
  );
}

export default App;
